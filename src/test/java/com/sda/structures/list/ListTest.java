package com.sda.structures.list;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ListTest {

    private List list;

    @Before
    public void init() {
        // TODO: use your own implementation
        // list = new ListImpl();
    }

    @Test
    public void testCreation() {
        Assert.assertNotNull(list);
        Assert.assertTrue(list.isEmpty());
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void testAppending() {
        Integer[] integers = { 10, 5, 17, 9 , 150, 33 };
        appendToList(integers, list);


        for (int i = 0; i < integers.length; i++) {
            Assert.assertEquals(integers[i], list.get(i));
        }
    }

    @Test
    public void testPrepending() {
        Integer[] integers = { 10, 5, 17, 9 , 150, 33 };
        prependToList(integers, list);

        for (int i = 0; i < integers.length; i++) {
            Assert.assertEquals(integers[integers.length - 1 - i], list.get(i));
        }
    }

    @Test
    public void testIsEmpty() {
        Assert.assertTrue(list.isEmpty());
        list.append(10);
        Assert.assertFalse(list.isEmpty());
    }

    @Test
    public void testSize() {
        Assert.assertEquals(0, list.size());
        list.append(10);
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void testGet() {
        Integer[] integers = { 10, 5, 6, 7, 8 };
        appendToList(integers, list);
        Assert.assertEquals(integers[integers.length/2], list.get(integers.length/2));
    }

    private static void appendToList(Integer[] items, List list) {
        for (Integer item : items) {
            list.append(item);
        }
    }

    private static void prependToList(Integer[] items, List list) {
        for (Integer item : items) {
            list.prepend(item);
        }
    }
}
