package com.sda.structures.stack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StackTest {
    Stack<Integer> stack;

    @Before
    public void init() {
        stack = new StackImpl<>(10);
    }

    @Test
    public void testCreation() {
        Assert.assertNotNull(stack);
        Assert.assertTrue(stack.isEmpty());
    }

    @Test
    public void testPeek() {
        final Integer[] testData = { 10, 20 , 30, 40, 50 };
        pushToStack(stack, testData);
        Assert.assertEquals(testData[testData.length-1], stack.peek());
        final int popTimes = 3;
        popStack(stack, popTimes);
        Assert.assertEquals(testData[testData.length-1-popTimes], stack.peek());
    }

    @Test
    public void testPop() {
        final Integer[] testData = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        pushToStack(stack, testData);
        for (int i = 0; i < testData.length; i++) {
            Assert.assertEquals(testData[testData.length-1-i], stack.peek());
            stack.pop();
        }
    }

    private void pushToStack(Stack<Integer> stack, Integer[] arr) {
        for (Integer item : arr) {
            stack.push(item);
        }
    }

    private void popStack(Stack<Integer> stack, int times) {
        while (times-- > 0) {
            stack.pop();
        }
    }
}
