package com.sda.algorithms.search;

import org.junit.Assert;
import org.junit.Test;

public class SearchTest {

    @Test
    public void testBinarySearch() {
        final Integer[] array = { 10, 20, 30, 40, 50, 60, 70 };
        final int key = 30;
        int index = BinarySearch.binarySearch(array, key);
        Assert.assertTrue(index >= 0);
        Assert.assertTrue(index < array.length);
        Assert.assertTrue(key == array[index]);

        index = BinarySearch.binarySearch(array, 100);
        Assert.assertTrue( index == -1);
    }
}
