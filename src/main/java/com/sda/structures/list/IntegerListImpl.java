package com.sda.structures.list;

public class IntegerListImpl implements IntegerList {

    Node head = null;
    int nodeCounter = 0;

    public void print() {
        Node current = head;
        while(current != null) {
            System.out.println(current.value);
            current = current.next;
        }
    }

    private boolean isValidPosition(int position) {
        return (position >= 0 && position < size());
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public int size() {
        return nodeCounter;
    }

    @Override
    public void prepend(Integer data) {
        Node newNode = new Node();
        newNode.value = data;
        newNode.next = head;
        head = newNode;
        nodeCounter++;
    }

    @Override
    public void append(Integer data) {
        Node newNode = new Node();
        newNode.value = data;
        if (head == null) {
            head = newNode;
        } else {
            Node current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        }
        nodeCounter++;
    }

    @Override
    public boolean insert(Integer data, int position) {
        if (!isValidPosition(position)) {
            return false;
        }
        Node previous = null;
        Node current = head;
        while(position > 0) {
            position--;
            previous = current;
            current = current.next;
        }
        if (null == previous) {
            prepend(data);
        } else {
            Node newNode = new Node();
            newNode.value = data;
            newNode.next = current;
            previous.next = newNode;
            nodeCounter++;
        }
        return true;
    }

    @Override
    public void remove(int position) {
        if (!isValidPosition(position)) {
            return;
        }
        Node previous = null;
        Node current = head;
        while(position > 0) {
            position--;
            previous = current;
            current = current.next;
        }
        if (null == previous) {
            head = head.next;
        } else {
            previous.next = previous.next.next;
        }
        nodeCounter--;
    }

    @Override
    public Integer get(int position) {
        if (!isValidPosition(position)) {
            return null;
        }
        Node current = head;
        while(position > 0) {
            position--;
            current = current.next;
        }
        return current.value;
    }

    @Override
    public void set(Integer data, int position) {
        if (!isValidPosition(position)) {
            return;
        }
        Node current = head;
        while(position > 0) {
            position--;
            current = current.next;
        }
        current.value = data;
    }
}
