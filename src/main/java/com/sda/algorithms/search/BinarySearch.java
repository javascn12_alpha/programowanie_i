package com.sda.algorithms.search;

public final class BinarySearch {
    /**
     * Binary search for a given key in a given array.
     *
     * @param dataset is an array of {@link Integer}
     * @param key is a value we want to find
     * @return index of a key if found or -1
     */
    static int binarySearch(Integer[] dataset, Integer key) {
        return -1;
    }
}
