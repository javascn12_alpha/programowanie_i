### Zadania ###

```
1. Wypisać na ekran następujące ciągi liczb:
    1, 3, 5, 7, 9, 11... 61
    0, 2, 4, 6, 4, 2, 0
    100, 10, 200, 20... 900, 90
    1, 1, 2, 3, 5, 8, 13, 21, 34... (ciąg Fibonacciego)
```
```
2. Rysowanie figur w konsoli:
    *         *         ******    *    *
    **         *        *    *     *  *
    ***         *       *    *      **
    ****         *      *    *      **
    *****         *     *    *     *  *
    ******         *    ******    *    *
   Krzyżyk dla chętnych
```
```
#!java
3. Zamienić miejscami elementy tablicy o indeksach 2 i 4:
    int[] myArray = new int[8];
    myArray[2] = 5;
    myArray[4] = 8;
```
```
4. Wykonać operacje na tablicach (po każdym punkcie wyświetlamy):
    • wypełniamy tablicę kolejnymi wartościami od 1 do 10
               [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    • co drugą liczbę zwiększamy o wartość jej poprzednika
              [0, 1, 2, 5, 4, 9, 6, 13, 8, 17]
    • każdą liczbę parzystą dzielimy przez 2
              [0, 1, 1, 5, 2, 9, 3, 13, 4, 17]
    • sumujemy wszystkie liczby w tablicy, wynik = ?
    • jaka będzie suma dla 100 elementów?
```
```
5. Utworzyć klasę RandomUtils która generuje tablicę wypełnioną losowymi wartościami.
```

```
6. Napisać funkcję która sprawdza czy dany String jest palindromem.
Przykłady danych testowych:
    • "kajak"
    • "Ikar bada braki"
    • "jaka piekna sobota"
    • "Akta generała ma mała renegatka"
```

```
7. Napisać funkcję sumującą dwie listy. Jeżeli listy mają różną długość sumujemy węzły o indeksach występujących w obydwu listach,
a nadmiarowe węzły z dłuższej listy dopisujemy na końcu.
```

```
8. Napisać funkcję, która wypisuje co n-ty element listy.
```

```
9. Rozszerzyć listę o metodę która zwraca 'head'. Następnie napisać funkcję, która znajdzie n-ty element list od końca.
Założenia:
    • przez listę można przejść tylko raz,
    • nie znamy rozmiaru listy / nie możemy skorzystać z funkcji sprawdzającej rozmiar listy.
    • jeśli lista jest krótsza niż żądany numer elementu, funkcja powinna zwracać null.
```

```
10. Napisać metodę, która odwraca listę rekurencyjnie.
```
```
11. Napisać metodę, która odwraca listę iteracyjnie.
```

```
12(*). Zaimplementuj stos za pomocą dwóch kolejek.
```

```
13(*). Zaimplementuj kolejkę za pomocą dwóch stosów.
```
```